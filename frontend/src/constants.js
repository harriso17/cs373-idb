import algoliasearch from 'algoliasearch/lite';

export const GITLAB_TKN = 'xjLubkBJgfcP1RcpzWd2';

export const GITLAB_BASE_URL = 'https://gitlab.com/api/v4/projects/21327660/';

export const BACKEND_TKN =
    'eyJhbGciOiJIUzUxMiJ9.eyJpZCI6Mn0.5AQM5goDM6PXvO9vnalE6njhKM_' +
    '4eU-8wGd-ZZqmbNiXARNY408umFzPnczywls51nuJCaDku4INzypRMk6umQ';

export const BACKEND_BASE_URL = 'https://api.powernet.energy/api/';

export const BACKEND_DEV_URL = 'localhost:5000/api/';

export const RESOURCES_URL =
    'https://powernet-resources-dev.s3.us-east-2.amazonaws.com/';

export const PROVIDER_BACKEND_BASE_URL = 'https://findeventsnear.me/api/';

export const GOOGLE_MAPS_KEY = 'AIzaSyCnKaqCKq71UYQwSpUH6mExyvcSgnDTs00';

const searchClient = algoliasearch(
    'M200BE97LL',
    'f6c1338129f932b7a857f35184b887a6'
);
export default searchClient;
