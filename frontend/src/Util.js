export function roundHit(hit) {
    var longt = hit._highlightResult.location.longitude.value;
    hit._highlightResult.location.longitude.value = roundNum(longt);
    var lat = hit._highlightResult.location.latitude.value;
    hit._highlightResult.location.latitude.value = roundNum(lat);
    return hit;
}
export function roundNum(n) {
    var num = parseFloat(n);
    var result = Math.round((num + Number.EPSILON) * 10000) / 10000;
    return result.toString();
}
