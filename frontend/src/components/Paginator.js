import React from 'react';
import '../App.css';
import { Pagination } from 'react-bootstrap';

/**
 *  Creates an array for looping in
 *  the body of paginator. Map requires
 *  this, for-loops will not work.
 */
const range = (start, end) =>
    Array.from({ length: end - start + 1 }, (_, i) => start + i);

/**
 *  Custom pagination component to be used by instant search.
 */
const Paginator = ({ padding = 2, refine, currentRefinement, nbPages }) => (
    // using bootstrap components to build this
    <Pagination>
        <Pagination.First onClick={() => refine(1)} />

        <Pagination.Prev
            onClick={() => refine(Math.max(currentRefinement - 1, 1))}
        />

        {range(
            Math.max(1, currentRefinement - padding),
            Math.min(nbPages, currentRefinement + padding)
        ).map((page) => (
            <Pagination.Item
                key={page}
                active={page === currentRefinement}
                onClick={() => refine(page)}
            >
                {page}
            </Pagination.Item>
        ))}

        <Pagination.Next
            onClick={() => refine(Math.min(currentRefinement + 1, nbPages))}
        />

        <Pagination.Last onClick={() => refine(nbPages)} />
    </Pagination>
);

export default Paginator;
