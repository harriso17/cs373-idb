import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import '../../App.css';
import { BACKEND_TKN, BACKEND_BASE_URL } from '../../constants';

/**
 *  Component that displays an instance of a policy.
 */
export default function Policy() {
    // stores passed in parameters
    let params = useParams();

    // string constants for fetch requests
    const policyIDFilter = '"name":"policy_id","op":"eq","val"';

    // variables for storing data from fetch requests
    const [policyData, setPolicyData] = useState({});

    // whenever params are altered, fetch new policy data
    useEffect(() => {
        // get policy data by using policy id filer
        fetch(
            new URL(
                BACKEND_BASE_URL +
                    'policy?q={"filters":[{' +
                    policyIDFilter +
                    ':"' +
                    params.id +
                    '"}]}'
            ),
            {
                method: 'GET',
                headers: new Headers({
                    Authorization: 'Bearer ' + BACKEND_TKN,
                }),
            }
        )
            .then((response) => response.json())
            .then((data) => setPolicyData(data));
    }, [params]);

    // if policy data is received, display the content
    function getContent() {
        if (policyData == null || policyData.objects == null) {
            return <div></div>;
        }

        let policy = policyData.objects[0];

        if (policy == null) {
            return <div></div>;
        }

        return (
            <Container>
                <div className="Model-Title">
                    <h1>{policy.name}</h1>
                    <h5>
                        <a href={policy.website_url}>Learn more...</a>
                    </h5>
                </div>
                <Row>
                    <Col>
                        <div className="Model-Title">
                            <p className="Model-Description">
                                {policy.description}
                            </p>
                        </div>
                    </Col>
                    <Col>
                        <div className="Model-Body">
                            <h5>
                                Commercial Energy Efficiency Incentives Program
                            </h5>
                            <Table variant="dark" className="Model-Table">
                                <tbody>
                                    <tr>
                                        <td>Year:</td>
                                        <td>{policy.created}</td>
                                    </tr>
                                    <tr>
                                        <td>Type:</td>
                                        <td>{policy.type_obj.name}</td>
                                    </tr>
                                    <tr>
                                        <td>Affected State:</td>
                                        <td>{policy.state.name}</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }

    return getContent();
}
