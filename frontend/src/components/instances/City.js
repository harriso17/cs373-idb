import React, { useState, useEffect } from 'react';
import { Col, Container, Row, Table } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import GoogleMapReact from 'google-map-react';
import '../../App.css';
import {
    BACKEND_TKN,
    BACKEND_BASE_URL,
    GOOGLE_MAPS_KEY,
} from '../../constants';
import { roundNum } from '../../Util';

/**
 *  Component that displays an instance of a city.
 */
export default function City() {
    // stores passed in parameters
    let params = useParams();

    // string constants with fetch requests
    const cityIDFilter = '"name":"city_id","op":"eq","val"';
    const stateNameFilter = '"name":"state","op":"eq","val"';
    const stateIDFilter = '"name":"state_id","op":"eq","val"';

    // variables for storing data from fetch requests
    const [cityData, setCityData] = useState({});
    const [emissionsData, setEmissionsData] = useState({});
    const [policyData, setPolicyData] = useState({});

    // whenever params are altered, fetch new city data
    useEffect(() => {
        // get city data by using city id filer
        fetch(
            new URL(
                BACKEND_BASE_URL +
                    'city?q={"filters":[{' +
                    cityIDFilter +
                    ':"' +
                    params.id +
                    '"}]}'
            ),
            {
                method: 'GET',
                headers: new Headers({
                    Authorization: 'Bearer ' + BACKEND_TKN,
                }),
            }
        )
            .then((response) => response.json())
            .then((data) => setCityData(data));
    }, [params]);

    // whenever data for city is received, check the data
    // and fetch emmissions and poly data
    useEffect(() => {
        if (cityData != null && cityData.objects != null) {
            // get the city data from the object
            const city = cityData.objects[0];

            // send request only if city data was found
            if (city != null) {
                // get emmisions data by using state id filter
                fetch(
                    new URL(
                        BACKEND_BASE_URL +
                            'emission?q={"filters":[{' +
                            stateNameFilter +
                            ':"' +
                            city.state_id +
                            '"}]}'
                    ),
                    {
                        method: 'GET',
                        headers: new Headers({
                            Authorization: 'Bearer ' + BACKEND_TKN,
                        }),
                    }
                )
                    .then((response) => response.json())
                    .then((data) => setEmissionsData(data));

                // get policy data by state id fileter
                fetch(
                    new URL(
                        BACKEND_BASE_URL +
                            'policy?q={"filters":[{' +
                            stateIDFilter +
                            ':"' +
                            city.state_id +
                            '"}]}'
                    ),
                    {
                        method: 'GET',
                        headers: new Headers({
                            Authorization: 'Bearer ' + BACKEND_TKN,
                        }),
                    }
                )
                    .then((response) => response.json())
                    .then((data) => setPolicyData(data));
            }
        }
    }, [cityData]);

    // returns emmisions table if emissions data is found
    function getEmissionTable() {
        if (emissionsData == null || emissionsData.objects == null) {
            return <div></div>;
        }

        let emissions = emissionsData.objects[0];

        if (emissions == null) {
            return <div></div>;
        }

        return (
            <div>
                <Table variant="dark" className="Model-Table">
                    <tbody>
                        <tr>
                            <th>CO2</th>
                            <th>{emissions.co2}</th>
                        </tr>
                        <tr>
                            <th>SO2</th>
                            <th>{emissions.so2}</th>
                        </tr>
                        <tr>
                            <th>NOx</th>
                            <th>{emissions.nox}</th>
                        </tr>
                    </tbody>
                </Table>
            </div>
        );
    }

    // returns policy table if policy data is found
    function getPolcy() {
        if (policyData == null || policyData.objects == null) {
            return <div></div>;
        }

        let policy = policyData.objects[0];

        if (policy == null) {
            return <div></div>;
        }

        return (
            <Table variant="dark" className="Model-Table">
                <tbody>
                    <tr>
                        <td>
                            <Link to={'/Policies/' + policy.policy_id}>
                                {policy.name}
                            </Link>
                        </td>
                    </tr>
                </tbody>
            </Table>
        );
    }

    // if city data is received, display the content
    function getContent() {
        if (cityData == null || cityData.objects == null) {
            return <div></div>;
        }

        let city = cityData.objects[0];

        if (city == null) {
            return <div></div>;
        }

        return (
            <Container>
                <div className="Model-Title">
                    <h1>{city.name}</h1>
                </div>
                <div className="Model-Body">
                    <img
                        className="mapbig"
                        src={city.location.image_url.replace(
                            's3://powernet-resources-dev',
                            'https://powernet-resources-dev.' +
                                's3.us-east-2.amazonaws.com'
                        )}
                        alt=""
                    />
                </div>
                <Row>
                    <Col>
                        <div className="Model-Body">
                            <h5>General Information</h5>
                            <Table variant="dark" className="Model-Table">
                                <tbody>
                                    <tr>
                                        <td>State</td>
                                        <td>{city.state.name}</td>
                                    </tr>
                                    <tr>
                                        <td>Population</td>
                                        <td>{city.population}</td>
                                    </tr>
                                    <tr>
                                        <td>Longitude</td>
                                        <td>
                                            {roundNum(city.location.longitude) +
                                                '° N'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Latitude</td>
                                        <td>
                                            {roundNum(city.location.latitude) +
                                                '° W'}
                                        </td>
                                    </tr>
                                </tbody>
                            </Table>
                            <h5>
                                State-Level Electric Power Industry Emissions
                            </h5>
                            <h5>(in Metric Tons)</h5>
                            {getEmissionTable()}
                            <h5>State-Level Policies</h5>
                            {getPolcy()}
                        </div>
                    </Col>
                    <Col>
                        <div style={{ height: '90%', width: '100%' }}>
                            <GoogleMapReact
                                bootstrapURLKeys={{ key: GOOGLE_MAPS_KEY }}
                                defaultCenter={{
                                    lat: city.location.latitude,
                                    lng: city.location.longitude,
                                }}
                                defaultZoom={12}
                            ></GoogleMapReact>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }

    return getContent();
}
