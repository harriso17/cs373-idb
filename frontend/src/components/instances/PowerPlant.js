import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import GoogleMapReact from 'google-map-react';
import '../../App.css';
import {
    BACKEND_TKN,
    BACKEND_BASE_URL,
    GOOGLE_MAPS_KEY,
} from '../../constants';
import { roundNum } from '../../Util';

/**
 *  Component that displays an instance of an organizaton.
 */
export default function PowerPlant() {
    // stores passed in parameters
    let params = useParams();

    // string constant for fetch requests
    const plantIDFilter = '"name":"plant_id","op":"eq","val"';
    const orgNameFiler = '"name":"name","op":"eq","val"';

    // variables for storing data from fetch requests
    const [plantData, setPlantData] = useState({});
    const [orgData, setOrgData] = useState({});

    // whenever params are altered, fetch new organization data
    useEffect(() => {
        fetch(
            new URL(
                BACKEND_BASE_URL +
                    'powerplant?q={"filters":[{' +
                    plantIDFilter +
                    ':"' +
                    params.id +
                    '"}]}'
            ),
            {
                method: 'GET',
                headers: new Headers({
                    Authorization: 'Bearer ' + BACKEND_TKN,
                }),
            }
        )
            .then((response) => response.json())
            .then((data) => setPlantData(data));
    }, [params]);

    useEffect(() => {
        if (plantData != null && plantData.objects != null) {
            const plant = plantData.objects[0];

            if (plant != null && plant.owner != null) {
                fetch(
                    new URL(
                        BACKEND_BASE_URL +
                            'organization?q={"filters":[{' +
                            orgNameFiler +
                            ':"' +
                            encodeURIComponent(plant.owner) +
                            '"}]}'
                    ),
                    {
                        method: 'GET',
                        headers: new Headers({
                            Authorization: 'Bearer ' + BACKEND_TKN,
                        }),
                    }
                )
                    .then((response) => response.json())
                    .then((data) => setOrgData(data));
            }
        }
    }, [plantData]);

    function getOrgInfo() {
        if (orgData == null || orgData.objects == null) {
            return <div></div>;
        }

        let org = orgData.objects[0];

        if (org == null) {
            if (plantData.objects[0].owner != null) {
                return <p>Owned by {plantData.objects[0].owner}</p>;
            }
            return <div></div>;
        }

        return (
            <p>
                Owned by
                <Link to={'/Organizations/' + org.organization_id}>
                    {' ' + org.name}
                </Link>
            </p>
        );
    }

    // if plant data is received, display the content
    function getContent() {
        if (plantData == null || plantData.objects == null) {
            return <div></div>;
        }

        let plant = plantData.objects[0];

        if (plant == null) {
            return <div></div>;
        }

        return (
            <Container>
                <div className="Model-Title">
                    <h1>{plant.plant_name}</h1>
                </div>
                <div className="Model-Body">
                    <img
                        className="mapbig"
                        src={plant.location.image_url.replace(
                            's3://powernet-resources-dev',
                            'https://powernet-resources-dev.' +
                                's3.us-east-2.amazonaws.com'
                        )}
                        alt=""
                    />
                </div>
                <div className="Model-Body">{getOrgInfo()}</div>
                <Row>
                    <Col>
                        <div className="Model-Body">
                            <h5> Stats </h5>
                            <Table variant="dark" className="Model-Table">
                                <thead>
                                    <tr>
                                        <th>Fuel</th>
                                        <th>Capacity (MW)</th>
                                        <th>Commissioning Year</th>
                                        <th>Latitude</th>
                                        <th>Longitude</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{plant.fuel}</td>
                                        <td>{plant.capacity}</td>
                                        <td>{plant.commission_year}</td>
                                        <td>
                                            {roundNum(plant.location.latitude) +
                                                '° W'}
                                        </td>
                                        <td>
                                            {roundNum(
                                                plant.location.longitude
                                            ) + '° N'}
                                        </td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </Col>
                    <Col>
                        <div style={{ height: '50vh', width: '100%' }}>
                            <GoogleMapReact
                                bootstrapURLKeys={{ key: GOOGLE_MAPS_KEY }}
                                defaultCenter={{
                                    lat: plant.location.latitude,
                                    lng: plant.location.longitude,
                                }}
                                defaultZoom={16}
                            ></GoogleMapReact>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }

    return getContent();
}
