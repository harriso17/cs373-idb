import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import '../../App.css';
import { BACKEND_TKN, BACKEND_BASE_URL, RESOURCES_URL } from '../../constants';

/**
 *  Component that displays an instance of an organizaton.
 */
export default function Organization() {
    // stores passed in parameters
    let params = useParams();

    // string constants for fetch requests
    const logoUrl = RESOURCES_URL + 'logos/';
    const orgIDFilter = '"name":"organization_id","op":"eq","val"';

    // variables for storing data from fetch requests
    const [orgData, setOrgData] = useState({});

    // whenever params are altered, fetch new organization data
    useEffect(() => {
        // get organization data by using organization id filer
        fetch(
            new URL(
                BACKEND_BASE_URL +
                    'organization?q={"filters":[{' +
                    orgIDFilter +
                    ':"' +
                    params.id +
                    '"}]}'
            ),
            {
                method: 'GET',
                headers: new Headers({
                    Authorization: 'Bearer ' + BACKEND_TKN,
                }),
            }
        )
            .then((response) => response.json())
            .then((data) => setOrgData(data));
    }, [params, orgIDFilter]);

    // check for null values in city
    function getCity(org) {
        if (!org || !org.city || !org.city.name) return <div>N/A</div>;

        if (!org.city.city_id) return <div>{org.city.name}</div>;

        return <Link to={'/Cities/' + org.city.city_id}>{org.city.name}</Link>;
    }

    // check for null values in state
    function getState(org) {
        if (!org || !org.state || !org.state.name) return 'N/A';

        return org.state.name;
    }

    // if org data is received, display the content
    function getContent() {
        if (orgData == null || orgData.objects == null) {
            return <div></div>;
        }

        let org = orgData.objects[0];

        if (org == null) {
            return <div></div>;
        }

        return (
            <div>
                <div className="Model-Title">
                    <h1>{org.name}</h1>
                </div>
                <div className="Model-Title">
                    <img
                        className="logobig"
                        src={logoUrl + org.logo_url}
                        alt=""
                    />
                </div>
                <div className="Model-Body">
                    <p
                        style={{
                            maxWidth: '500px',
                            textAlign: 'center',
                            margin: 'auto',
                        }}
                    >
                        {org.description}
                    </p>
                    <br />
                    <p>
                        Learn more at
                        <a href={'https://' + org.website}>
                            {' ' + org.website}
                        </a>
                    </p>
                </div>
                <div className="Model-Body">
                    <Table variant="dark" className="Model-Table">
                        <thead>
                            <tr>
                                <th>City</th>
                                <th>State</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{getCity(org)}</td>
                                <td>{getState(org)}</td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            </div>
        );
    }

    return getContent();
}
