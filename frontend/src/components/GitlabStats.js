import React from 'react';
import Table from 'react-bootstrap/Table';
import { GITLAB_TKN, GITLAB_BASE_URL } from '../constants';

/**
 *  Component that displays our gitlab repo's
 *  overall stats.
 */
class GitlabStats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            commits: 0,
            issues: 0,
            unit_tests: 0,
            return_size: 0,
        };
    }

    componentDidMount() {
        var bearer = 'Bearer ' + GITLAB_TKN;
        var page_num = 1;
        var num_pages = 5;

        while (num_pages > 0) {
            var url = new URL(GITLAB_BASE_URL + 'repository/commits');
            var params = {
                all: true,
                id: 21327660,
                order: 'default',
                page: page_num,
                pages: 100,
                per_page: 100,
            };
            url.search = new URLSearchParams(params).toString();

            fetch(url, {
                method: 'GET',
                headers: new Headers({ Authorization: bearer }),
            })
                .then((response) => {
                    var j_data = response.json();
                    return j_data;
                })
                .then(
                    (result) => {
                        this.setState({
                            isLoaded: true,
                            commits: this.state.commits + result.length,
                            return_size: result.length,
                        });
                    },
                    // Note: it's important to handle errors here
                    // instead of a catch() block so that we don't swallow
                    // exceptions from actual bugs in components.
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error,
                        });
                    }
                );

            page_num++;
            num_pages--;
        }

        url = new URL(GITLAB_BASE_URL + 'issues');
        params = { scope: 'all', per_page: 100 };
        url.search = new URLSearchParams(params).toString();

        fetch(url, {
            method: 'GET',
            headers: new Headers({ Authorization: bearer }),
        })
            .then((res) => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        issues: result.length,
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error,
                    });
                }
            );
    }

    componentWillUnmount() {
        // fix Warning: Can't perform a React state update on
        // an unmounted component
        this.setState = (state, callback) => {
            return;
        };
    }

    render() {
        const { error, isLoaded, commits, issues, unit_tests } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div>
                    <Table variant="dark">
                        <thead>
                            <tr>
                                <th>No. Commits</th>
                                <th>No. Issues</th>
                                <th>No. Unit Tests</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{commits}</td>
                                <td>{issues}</td>
                                <td>{unit_tests}</td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            );
        }
    }
}

export { GitlabStats };
