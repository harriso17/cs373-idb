import React from 'react';
import { Dropdown } from 'react-bootstrap';

/**
 *  Defines a custom filter widget for instant search.
 */
const Filter = ({ items, refine, name }) => (
    <Dropdown style={{ paddingRight: '5px' }}>
        <Dropdown.Toggle variant="primary" id="dropdown-basic">
            {name}
        </Dropdown.Toggle>
        <Dropdown.Menu>
            {items.map((item) => (
                <Dropdown.Item
                    key={item.label}
                    href="#"
                    onClick={(event) => {
                        event.preventDefault();
                        refine(item.value);
                    }}
                    style={{ fontWeight: item.isRefined ? 'bold' : '' }}
                >
                    {item.label} ({item.count})
                </Dropdown.Item>
            ))}
        </Dropdown.Menu>
    </Dropdown>
);

export default Filter;
