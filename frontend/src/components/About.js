import React from 'react';
import logo from '../logo.svg';
import '../App.css';
import { MemberStats } from './MemberStats';
import { GitlabStats } from './GitlabStats';
import { Container, Row, Col, Card, Table } from 'react-bootstrap';
import { RESOURCES_URL } from '../constants';

/**
 *  The about page compnent that displays
 *  all the team member and project info.
 */
export default function About() {
    function getCard(name, img, bio, email, username, linkedIn, role) {
        return (
            <Card style={{ height: '100%', aspectRatio: 1, padding: '5px' }}>
                <Card.Header>
                    <img
                        src={RESOURCES_URL + img}
                        alt=""
                        style={{ width: '100%', aspectRatio: 1 }}
                    />
                </Card.Header>
                <Card.Body>
                    <a href={linkedIn}>
                        <Card.Title className="mt-1">{name}</Card.Title>
                    </a>
                    <Card.Subtitle className="text-muted">
                        {role}
                    </Card.Subtitle>
                    <Card.Text className="mt-1 text-dark">{bio}</Card.Text>
                </Card.Body>
                <Card.Footer>
                    <MemberStats email={email} username={username} />
                </Card.Footer>
            </Card>
        );
    }

    return (
        <div className="Model-Body">
            <div className="Model-Title">
                <img src={logo} className="App-logo" alt="" />
            </div>
            <div className="Model-Table">
                <h1>Description</h1>
                <p className="Model-Description">
                    PowerNet is a website designed to educate citizens about
                    energy companies all over the U.S so that people can make
                    smarter, more informed, decisions regarding the energy
                    consumption. In addition PowerNet provides information about
                    legislative policies which may allow for citizens to take
                    advantage of the benefits of switching to greener energy
                    sources.
                </p>
                <br />
                <h1>About Our Data</h1>
                <p className="Model-Description">
                    The data we gather is centered on giving accurate
                    information on energy infrastructure around the United
                    States. The intersection of location, companies, and
                    policies provides a great repsentation of any location's
                    energy providers. The data provides insight into certain
                    trends in places where different policies and companies
                    meet.
                </p>
                <div className="Model-Title">
                    <h1 className="text-center text-white">Team Members</h1>
                </div>
                <Container>
                    <Row>
                        <Col>
                            {getCard(
                                'Alexander Greason',
                                'alexgreason.jpg',
                                "I'm Alex, I am presently in my senior year, " +
                                    "double-majoring in CS and Math. I am interested " +
                                    "in renewable energy, space tech, semiconductor " +
                                    "engineering, cellular automata and machine " +
                                    "learning. My hobbies are reading fanfiction and " +
                                    "programming.",
                                "alexgreason@utexas.edu, quantum935@yahoo.com",
                                "alexgreason1",
                                "https://www.linkedin.com/in/alex-greason-43553691/",
                                "Fullstack Developer"
                            )}
                        </Col>
                        <Col>
                            {getCard(
                                'Harrison Gross',
                                'harrison.jpg',
                                "I'm a senior computer science major. In my spare " +
                                    "time I like to play basketball and videogames.",
                                "harrisongross17@gmail.com",
                                "harriso17",
                                "https://www.linkedin.com/in/harrison-gross-555749183/",
                                "Fullstack Developer"
                            )}
                        </Col>
                        <Col>
                            {getCard(
                                'Sean Chen',
                                'sean.jpg',
                                "I'm a third year Computer Science student at UT " +
                                    "Austin. Most of the time I spend outside of CS " +
                                    "is in CSGO (and other video games).",
                                "sean.chen@utexas.edu, sean@Seans-Mac-Pro.local",
                                "sean.chen",
                                "https://www.linkedin.com/in/sean-chen-444737b5/",
                                "Fullstack Developer"
                            )}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            {getCard(
                                'Travis Eakin',
                                'travis.png',
                                "Hello! I'm Travis. I'm currently a senior " +
                                    "majoring in Computer Science. In my spare " +
                                    "time, I enjoy cycling and game development.",
                                "travis.eakin@utexas.edu",
                                "eakintk",
                                "https://www.linkedin.com/in/travis-eakin-49309b185/",
                                "Fullstack Developer / Team Leader"
                            )}
                        </Col>
                        <Col>
                            {getCard(
                                'Carlo DeSantis',
                                'cdesantis.jpeg',
                                "My name is Carlo. I'm currently a junior here " +
                                    "at UT. Outside of classes I like to spend my " +
                                    "time watching movies or playing guitar.",
                                "cdesantis@tanklogix.com, cdesantis@utexas.edu",
                                "cdesan",
                                "https://www.linkedin.com/in/carlo-desantis-785563189/",
                                "Fullstack Developer"
                            )}
                        </Col>
                    </Row>
                </Container>
                <h2>Project Stats</h2>
                <GitlabStats />
                <h2>Tools Used</h2>
                <Table variant="dark">
                    <thead className="text-left">
                        <tr>
                            <th>Tool</th>
                            <th>Use</th>
                        </tr>
                    </thead>
                    <tbody className="text-left">
                        <tr>
                            <td>Gitlab</td>
                            <td>
                                We use Gitlab to host our source code for this
                                project as well as utilize Gitlab's issue
                                tracking to provide communication of tasks
                                between team members.
                            </td>
                        </tr>
                        <tr>
                            <td>Amazon Amplify</td>
                            <td>
                                We use Amplify to host our website on the
                                internet.
                            </td>
                        </tr>
                        <tr>
                            <td>React</td>
                            <td>
                                We use React as the framework for the basic
                                functionality of our web app.
                            </td>
                        </tr>
                        <tr>
                            <td>React Router</td>
                            <td>
                                We use React router to provide routing for the
                                web app.
                            </td>
                        </tr>
                        <tr>
                            <td>React Player</td>
                            <td>
                                We use React player to support embedded videos
                                in various pages.
                            </td>
                        </tr>
                        <tr>
                            <td>Bootstrap</td>
                            <td>
                                We use Bootstrap as the styling framework of the
                                site.
                            </td>
                        </tr>
                        <tr>
                            <td>Postman</td>
                            <td>
                                We use Postman for developing and testing out
                                our own API.
                            </td>
                        </tr>
                        <tr>
                            <td>Slack</td>
                            <td>
                                We use Slack for direct communication between
                                team members via messaging.
                            </td>
                        </tr>
                        <tr>
                            <td>Zoom</td>
                            <td>We use Zoom for our weekly team meetings.</td>
                        </tr>
                        <tr>
                            <td>Namecheap</td>
                            <td>
                                We used to obtain our domain Powernet.energy.
                            </td>
                        </tr>
                    </tbody>
                </Table>
                <h2>Data Sources</h2>
                <Table variant="dark">
                    <thead className="text-left">
                        <tr>
                            <th>Data Source</th>
                            <th>Scrape Method</th>
                        </tr>
                    </thead>
                    <tbody className="text-left">
                        <tr>
                            <td>
                                <a href="https://www.eia.gov/opendata/commands.php">
                                    <img
                                        src={RESOURCES_URL + 'eia.png'}
                                        height="40"
                                        alt="US Energy"
                                    />{' '}
                                    US Energy Information Administration
                                </a>
                            </td>
                            <td>Data aquired via RESTful API calls</td>
                        </tr>
                        <tr>
                            <td>
                                <a
                                    href={
                                        'https://nsrdb.nrel.gov/data-sets/' +
                                        'api-instructions.html'
                                    }
                                >
                                    <img
                                        src={RESOURCES_URL + 'nrel.jpg'}
                                        height="40"
                                        alt="National Solar"
                                    />{' '}
                                    National Solar Radiation Database
                                </a>
                            </td>
                            <td>Data aquired via RESTful API calls</td>
                        </tr>
                        <tr>
                            <td>
                                <a
                                    href={
                                        'https://developers.google.com/' +
                                        'earth-engine/#api'
                                    }
                                >
                                    <img
                                        src={RESOURCES_URL + 'google-earth.png'}
                                        height="40"
                                        alt="Google Earth"
                                    />{' '}
                                    Google Earth Engine
                                </a>
                            </td>
                            <td>Data aquired via RESTful API calls</td>
                        </tr>
                        <tr>
                            <td>
                                <a
                                    href={
                                        'https://www.census.gov/data/developers/' +
                                        'data-sets/popest-popproj/popest.html'
                                    }
                                >
                                    <img
                                        src={RESOURCES_URL + 'uscb.png'}
                                        height="40"
                                        alt="US Census"
                                    />{' '}
                                    US Census Bureau
                                </a>
                            </td>
                            <td>Data aquired via RESTful API calls</td>
                        </tr>
                    </tbody>
                </Table>
                <a href="https://gitlab.com/harriso17/cs373-idb">
                    <h3>
                        <img
                            src={RESOURCES_URL + 'gitlab.png'}
                            height="40"
                            alt="Gitlab"
                        />
                        Gitlab Repo
                    </h3>
                </a>
                <a href="https://documenter.getpostman.com/view/12849192/TVRrVjg7">
                    <h3>
                        <img
                            src={RESOURCES_URL + 'postman.png'}
                            height="40"
                            alt="Postman"
                        />
                        Postman Docs
                    </h3>
                </a>
            </div>
        </div>
    );
}
