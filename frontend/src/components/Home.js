import React from 'react';
import { Button } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import logo from '../logo.svg';
import '../App.css';

/**
 * Defines the home/splash page component.
 */
export default function Home() {
    return (
        <div>
            <div className="Model-Title">
                {/* Tag to display the main logo */}
                <img
                    className="App-logo"
                    src={logo}
                    alt="Component Not Found"
                />
            </div>
            <div className="Model-Body">
                <p className="text-white">
                    Find information about all things power!
                </p>
                <p className="text-white">
                    Click below to find our more about everything from
                    organizations to policies.
                </p>
                <div>
                    <LinkContainer to="/Organizations">
                        <Button variant="primary">Organizations</Button>
                    </LinkContainer>
                    {'   '}
                    <LinkContainer to="/PowerPlants">
                        <Button variant="warning">Power Plants</Button>
                    </LinkContainer>
                    {'   '}
                    <LinkContainer to="/Cities">
                        <Button variant="success">Cities</Button>
                    </LinkContainer>
                    {'   '}
                    <LinkContainer to="/Policies">
                        <Button variant="secondary">Policies</Button>
                    </LinkContainer>
                </div>
            </div>
        </div>
    );
}
