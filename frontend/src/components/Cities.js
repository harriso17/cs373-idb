import React from 'react';
import '../App.css';
import searchClient from '../constants';
import PagingTable from './PagingTable';
import Filter from './Filter';
import Paginator from './Paginator';
import ClearButton from './ClearSearch';
import { Link } from 'react-router-dom';
import {
    InstantSearch,
    SearchBox,
    Highlight,
    Configure,
    connectHits,
    connectPagination,
    connectRefinementList,
    connectCurrentRefinements,
    Stats,
    SortBy,
} from 'react-instantsearch-dom';
import { roundHit } from '../Util';

/**
 *  Component that displays pages of organizatons.
 */
export default function Cities() {
    // creates a custom table, data is recieved in hits
    // uses paging table to format everything
    const Hits = ({ hits }) => (
        <PagingTable
            header={['Name', 'State', 'Population', 'Latitude', 'Longitude']}
            body={hits.map((hit) => [
                <Link to={'Cities/' + hit.city_id}>
                    {<Highlight attribute="name" hit={hit} />}
                </Link>,
                <div>{<Highlight attribute="state.name" hit={hit} />}</div>,
                <div>{<Highlight attribute="population" hit={hit} />}</div>,
                <div>
                    {
                        <Highlight
                            attribute="location.latitude"
                            hit={roundHit(hit)}
                        />
                    }{' '}
                    ° W
                </div>,
                <div>
                    {
                        <Highlight
                            attribute="location.longitude"
                            hit={roundHit(hit)}
                        />
                    }{' '}
                    ° N
                </div>,
            ])}
        />
    );

    // connect custom components to instant search
    const CustomHits = connectHits(Hits);
    const CustomPaging = connectPagination(Paginator);
    const CustomFilter = connectRefinementList(Filter);
    const CustomClearRefinements = connectCurrentRefinements(ClearButton);

    // return the component to be rendered
    return (
        <div>
            <div className="Model-Title">
                <h1>Cities</h1>
            </div>
            <div className="Model-Table">
                <InstantSearch searchClient={searchClient} indexName="cities">
                    {/* only display 8 items per page */}
                    <Configure hitsPerPage={8} />

                    {/* search box component */}
                    <SearchBox translations={{ placeholder: 'Search...' }} />
                    <div className="Horizontal-Block">
                        <CustomFilter attribute="state.name" name="States" />
                        <CustomClearRefinements />
                    </div>
                    <div className="Model-Table">
                        <SortBy
                            defaultRefinement="cities"
                            items={[
                                { value: 'cities', label: 'Most Relevant' },
                                { value: 'cities-a-z', label: 'A-Z' },
                                { value: 'cities-z-a', label: 'Z-A' },
                                {
                                    value: 'cities-pop-asc',
                                    label: 'population asc.',
                                },
                                {
                                    value: 'cities-pop-desc',
                                    label: 'population desc.',
                                },
                            ]}
                        />
                        <Stats />
                    </div>
                    {/* these are custom table and paging components */}
                    <CustomHits />
                    <CustomPaging />
                </InstantSearch>
            </div>
        </div>
    );
}
