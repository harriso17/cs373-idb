import React from 'react';
import '../App.css';
import { Table } from 'react-bootstrap';

/**
 *  Component that displays a table. 2 parameters
 *  are required: header, body. header is a list
 *  of titles for the Table's head. Body must
 *  be a list of entries, where eahc entry is
 *  a list of components that align to the header
 *  fields.
 *
 *  Example:
 *      header = ["Name", "id"]
 *      body = [
 *          [<h1>Entry 1</h1>, "1"],
 *          [<h1>Entry 2</h1>, "2"]
 *      ]
 */
export default function PagingTable(props) {
    return (
        // create the table
        <Table variant="dark" hover className="Model-Table">
            {/* establish the header of the table */}
            <thead>
                <tr>
                    {props.header.map((title, index) => (
                        <th key={index}>{title}</th>
                    ))}
                </tr>
            </thead>
            {/* populate the body by looping through body array */}
            <tbody>
                {props.body.map((entry, index1) => (
                    <tr key={index1}>
                        {entry.map((data, index2) => (
                            <td key={index2}>{data}</td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </Table>
    );
}
