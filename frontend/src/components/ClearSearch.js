import React from 'react';
import { Button } from 'react-bootstrap';

const ClearButton = ({ items, refine }) => {
    return (
        <Button
            variant="outline-danger"
            onClick={() => refine(items)}
            disabled={!items.length}
        >
            Clear
        </Button>
    );
};

export default ClearButton;
