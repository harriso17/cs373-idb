import React from 'react';
import '../App.css';
import searchClient from '../constants';
import PagingTable from './PagingTable';
import Filter from './Filter';
import Paginator from './Paginator';
import ClearButton from './ClearSearch';
import { Link } from 'react-router-dom';
import {
    InstantSearch,
    SearchBox,
    Highlight,
    Configure,
    connectHits,
    connectPagination,
    connectRefinementList,
    connectCurrentRefinements,
    Stats,
    SortBy,
} from 'react-instantsearch-dom';
import { roundHit } from '../Util';

/**
 *  Component that displays pages of organizatons.
 */
export default function PowerPlants() {
    // creates a custom table, data is recieved in hits
    // uses paging table to format everything
    const Hits = ({ hits }) => (
        <PagingTable
            header={['Name', 'Fuel', 'Owner', 'Latitude', 'Longitude']}
            body={hits.map((hit) => [
                <Link to={'PowerPlants/' + hit.plant_id}>
                    {<Highlight attribute="plant_name" hit={hit} />}
                </Link>,
                <div>{<Highlight attribute="fuel" hit={hit} />}</div>,
                <div>{<Highlight attribute="owner" hit={hit} />}</div>,
                <div>
                    {
                        <Highlight
                            attribute="location.latitude"
                            hit={roundHit(hit)}
                        />
                    }
                    ° W
                </div>,
                <div>
                    {
                        <Highlight
                            attribute="location.longitude"
                            hit={roundHit(hit)}
                        />
                    }
                    ° N
                </div>,
            ])}
        />
    );

    // connect custom components to instant search
    const CustomHits = connectHits(Hits);
    const CustomPaging = connectPagination(Paginator);
    const CustomFilter = connectRefinementList(Filter);
    const CustomClearRefinements = connectCurrentRefinements(ClearButton);

    // return the component to be rendered
    return (
        <div>
            <div className="Model-Title">
                <h1>PowerPlants</h1>
            </div>
            <div className="Model-Table">
                <InstantSearch
                    searchClient={searchClient}
                    indexName="powerplants"
                >
                    {/* only display 8 items per page */}
                    <Configure hitsPerPage={8} />

                    {/* search box component */}
                    <SearchBox translations={{ placeholder: 'Search...' }} />
                    <div className="Horizontal-Block">
                        <CustomFilter attribute="fuel" name="Fuels" />
                        <CustomFilter attribute="owner" name="Owners" />
                        <CustomClearRefinements />
                    </div>
                    <div className="Model-Table">
                        <SortBy
                            defaultRefinement="powerplants"
                            items={[
                                {
                                    value: 'powerplants',
                                    label: 'Most Relevant',
                                },
                                {
                                    value: 'powerplants-a-z',
                                    label: 'A-Z',
                                },
                                {
                                    value: 'powerplants-z-a',
                                    label: 'Z-A',
                                },
                                {
                                    value: 'powerplants-fuel-a-z',
                                    label: 'Fuel A-Z',
                                },
                                {
                                    value: 'powerplants-fuel-z-a',
                                    label: 'Fuel Z-A',
                                },
                            ]}
                        />
                        <Stats />
                    </div>
                    {/* these are custom table and paging components */}
                    <CustomHits />
                    <CustomPaging />
                </InstantSearch>
            </div>
        </div>
    );
}
