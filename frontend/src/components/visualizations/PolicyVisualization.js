import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import axios from 'axios';
import { Pie, PieChart, Legend, Tooltip, Cell } from 'recharts';
import { BACKEND_BASE_URL, BACKEND_TKN } from '../../constants';

const PolicyVisualization = () => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState();

    useEffect(() => {
        var data = {};
        var config = {
            method: 'get',
            url: BACKEND_BASE_URL + 'policy',
            headers: { Authorization: 'Bearer ' + BACKEND_TKN },
        };

        axios(config).then(function (response) {
            var promises = [];
            //collect data from api
            for (var i = 1; i <= response.data.total_pages; i++) {
                config = {
                    method: 'get',
                    url: BACKEND_BASE_URL + 'policy?page=' + i,
                    headers: { Authorization: 'Bearer ' + BACKEND_TKN },
                };
                promises.push(
                    axios(config).then(function (page) {
                        page.data.objects.forEach((element) => {
                            if (element.type_obj.name in data) {
                                data[element.type_obj.name] += 1;
                            } else {
                                data[element.type_obj.name] = 1;
                            }
                        });
                    })
                );
            }
            Promise.all(promises).then(function () {
                //form data for Recharts
                var output = [];
                var other_total = 0;
                for (let key in data) {
                    if (data[key] < 120) {
                        other_total += data[key];
                    } else {
                        output.push({
                            name: key,
                            value: data[key],
                        });
                    }
                }
                output.push({
                    name: 'Other',
                    value: other_total,
                });
                setData(output);
                setLoading(false);
            });
        });
    }, []);

    if (isLoading) {
        return (
            <div className="Model-Title">
                <Spinner animation="border" variant="light"></Spinner>
            </div>
        );
    }

    //color set for pie slices
    var colors = [
        '#4287f5',
        '#42f575',
        '#81f542',
        '#e0f542',
        '#f5a142',
        '#f54242',
        '#f54296',
        '#e042f5',
        '#4242f5',
    ];
    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: 'white',
            }}
        >
            <PieChart width={600} height={600}>
                <Pie
                    data={data}
                    dataKey="value"
                    nameKey="name"
                    cx="50%"
                    cy="50%"
                    outerRadius={175}
                    label
                >
                    {data.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={colors[index]} />
                    ))}
                </Pie>
                <Legend />
                <Tooltip />
            </PieChart>
        </div>
    );
};

export default PolicyVisualization;
