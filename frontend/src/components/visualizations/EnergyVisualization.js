import React from 'react';
import * as d3 from 'd3';

class EnergyVisualization extends React.Component {
    constructor(props) {
        super(props);
        this.CreateChart = this.CreateChart.bind(this);
    }

    componentDidMount() {
        this.CreateChart();
    }

    CreateChart() {
        let data = {
            children: [
                { EnergyType: 'Solar', value: 2290 },
                { EnergyType: 'Gas', value: 1741 },
                { EnergyType: 'Hydro', value: 1457 },
                { EnergyType: 'Geothermal', value: 65 },
                { EnergyType: 'Wind', value: 1043 },
                { EnergyType: 'Oil', value: 847 },
                { EnergyType: 'Waste', value: 567 },
                { EnergyType: 'Coal', value: 338 },
                { EnergyType: 'Biomass', value: 156 },
                { EnergyType: 'Nuclear', value: 61 },
            ],
        };

        let diameter = 500;

        const colorScale = d3
            .scaleSequential()
            .interpolator(d3.interpolateWarm)
            .domain([0, data.children.length]);

        let bubble = d3.pack().size([diameter, diameter]).padding(5);

        let margin = {
            left: 0,
            right: 500,
            top: 0,
            bottom: 0,
        };

        let svg = d3
            .select('#EnergyTypeBubbleChart')
            .append('svg')
            .attr(
                'viewBox',
                '-250 0 ' + (diameter + margin.right) + ' ' + diameter
            )
            .attr('preserveAspectRatio', 'xMinYMin meet')
            .attr('class', 'chart-svg');

        let root = d3
            .hierarchy(data)
            .sum(function (d) {
                return d.value;
            })
            .sort(function (a, b) {
                return b.value - a.value;
            });

        bubble(root);

        let node = svg
            .selectAll('.node')
            .data(root.children)
            .enter()
            .append('g')
            .attr('class', 'node')
            .attr('transform', function (d) {
                return 'translate(' + d.x + ' ' + d.y + ')';
            })
            .append('g')
            .attr('class', 'graph');

        // Create bubble
        node.append('circle')
            .attr('r', function (d) {
                return d.r;
            })
            .style('fill', (_, i) => colorScale(i));

        //add energy type to bubble
        node.append('text')
            .style('text-anchor', 'middle')
            .text(function (d) {
                return d.data.EnergyType;
            })
            .style('fill', '#FFF');

        //add number of plants to bubble
        node.append('text')
            .attr('dy', '1em')
            .style('text-anchor', 'middle')
            .text(function (d) {
                return d.data.value;
            })
            .style('fill', '#FFF');
    }
    render() {
        return <div className="vis-container" id="EnergyTypeBubbleChart"></div>;
    }
}
export default EnergyVisualization;
