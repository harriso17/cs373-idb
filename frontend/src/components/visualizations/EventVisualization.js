import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import axios from 'axios';
import { AreaChart, Area, XAxis, YAxis, Tooltip } from 'recharts';
import { PROVIDER_BACKEND_BASE_URL } from '../../constants';

const EventVisualization = () => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState();

    useEffect(() => {
        //time period to look through
        var data = {
            '2020-10': 0,
            '2020-11': 0,
            '2020-12': 0,
            '2021-01': 0,
            '2021-02': 0,
            '2021-03': 0,
            '2021-04': 0,
            '2021-05': 0,
            '2021-06': 0,
            '2021-07': 0,
            '2021-08': 0,
            '2021-09': 0,
            '2021-10': 0,
            '2021-11': 0,
            '2021-12': 0,
        };
        var promises = [];
        for (let month in data) {
            var config = {
                method: 'get',
                url:
                    PROVIDER_BACKEND_BASE_URL +
                    'events?date=' +
                    month +
                    '&orderBy=venue_id',
                headers: {},
            };
            promises.push(
                axios(config).then(function (response) {
                    if (response.data.total_pages !== 0) {
                        data[month] += (response.data.total_pages - 1) * 10;
                        var subconfig = {
                            method: 'get',
                            url:
                                PROVIDER_BACKEND_BASE_URL +
                                'events?page_number=' +
                                response.data.total_pages +
                                '&date=' +
                                month +
                                '&orderBy=venue_id',
                            headers: {},
                        };

                        promises.push(
                            axios(subconfig).then(function (lastpage) {
                                data[month] += lastpage.data.data.length;
                            })
                        );
                    }
                })
            );
        }
        Promise.all(promises).then(function () {
            //form data for Recharts
            var output = [];
            for (let key in data) {
                output.push({
                    name: key,
                    events: data[key],
                });
            }
            setData(output);
            setLoading(false);
        });
    }, []);

    if (isLoading) {
        return (
            <div className="Model-Title">
                <Spinner animation="border" variant="light"></Spinner>
            </div>
        );
    }

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: 'white',
            }}
        >
            <AreaChart
                width={730}
                height={250}
                data={data}
                margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
            >
                <defs>
                    <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                        <stop
                            offset="5%"
                            stopColor="#82ca9d"
                            stopOpacity={0.8}
                        />
                        <stop
                            offset="95%"
                            stopColor="#82ca9d"
                            stopOpacity={0}
                        />
                    </linearGradient>
                </defs>
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Area
                    type="monotone"
                    dataKey="events"
                    stroke="#82ca9d"
                    fillOpacity={1}
                    fill="url(#colorPv)"
                />
            </AreaChart>
        </div>
    );
};

export default EventVisualization;
