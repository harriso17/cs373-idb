import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import axios from 'axios';
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
  } from 'recharts';
import { PROVIDER_BACKEND_BASE_URL } from '../../constants';

const NewsVisualization = () => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState();

    useEffect(() => {
        var city_to_data = {"Dallas": 0, "Austin": 0, "Houston": 0, "San_Antonio": 0};
        var cities = ["Dallas", "Austin", "Houston", "San_Antonio"];
        var promises = [];

        for(var i = 0; i < cities.length; i++){
            var city = cities[i];
            var cur_page = 1;
            while(cur_page < 20){
                var config = {
                    method: 'get',
                    url: PROVIDER_BACKEND_BASE_URL + 'news',
                    params: {query: city, page_number: cur_page}
                };
                promises.push(axios(config).then(function (response) {
                    //collect data from api
                    var c = response["config"]["params"]["query"];
                    var count = response.data["data"].length;
                    city_to_data[c] += count;
                }).catch(function (response) {

                }));
                cur_page++;

            }

        }
        Promise.all(promises).then(function () {
            //form data for Recharts
            var output = [];
            for(let k in city_to_data){
                output.push({"City": k, "News Events": city_to_data[k]});
            }
            setData(output);
            setLoading(false);
        });
    }, []);

    if (isLoading) {
        return (
            <div className="Model-Title">
                <Spinner animation="border" variant="light"></Spinner>
            </div>
        );
    }

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: 'white',
            }}
        >
            <BarChart
                width={900}
                height={400}
                data={data}
                margin={{
                top: 5, right: 30, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="City" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="News Events" fill='#82ca9d' />
            </BarChart>
        </div>
    );
};

export default NewsVisualization;