import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import axios from 'axios';
import {
    PieChart, Pie, Cell, Legend, Tooltip
  } from 'recharts';
import { PROVIDER_BACKEND_BASE_URL } from '../../constants';

const VenueVisualization = () => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState();

    useEffect(() => {
        let acc_count = 0;
        let non_acc_count = 0;
        var promises = [];
        var cur_page = 1;
            while(cur_page < 10){
                var config = {
                    method: 'get',
                    url: PROVIDER_BACKEND_BASE_URL + 'venues',
                    params: {page_number: cur_page}
                };
                promises.push(axios(config).then(function (response) {
                    //collect data from api
                    var venues = response.data.data;
                    for(var i = 0; i < venues.length; i++){
                        var acc_info = venues[i]["accessible_seating_info"];
                        if(acc_info === "N/A"){
                            non_acc_count++;
                        }
                        else{
                            acc_count++;
                        }
                    }
                }).catch(function (response) {

                }));
                cur_page++;

            }
        Promise.all(promises).then(function () {
            //form data for Recharts
            var output = null
            if(acc_count !== 0 && non_acc_count !== 0){
                output = [{name: "Accessible Seating Info", value: acc_count}, {name: "No Information", value: non_acc_count}];
            }
            setData(output);
            setLoading(false);
        });
    }, []);

    if (isLoading) {
        return (
            <div className="Model-Title">
                <Spinner animation="border" variant="light"></Spinner>
            </div>
        );
    }
    else if(isLoading === false && data === null){
        return (
            <div className="Model-Body">
                <h2>Unable to retrieve information.</h2>
            </div>
        );
    }
    var colors = [
        '#4287f5',
        '#42f575',
    ];
    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: 'white',
            }}
        >
            <PieChart width={600} height={600}>
                <Pie
                    data={data}
                    dataKey="value"
                    nameKey="name"
                    cx="50%"
                    cy="50%"
                    outerRadius={175}
                    label
                >
                    {data.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={colors[index]} />
                    ))}
                </Pie>
                <Legend />
                <Tooltip />
            </PieChart>
        </div>
    );
};

export default VenueVisualization;