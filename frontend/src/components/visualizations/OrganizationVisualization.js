import React, { useEffect, useState } from 'react';
import { Spinner } from 'react-bootstrap';
import axios from 'axios';
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
  } from 'recharts';
import { BACKEND_BASE_URL, BACKEND_TKN } from '../../constants';

const OrganizationVisualization = () => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState();

    useEffect(() => {
        var data = {};
        var id_to_name = {};
        var config = {
            method: 'get',
            url: BACKEND_BASE_URL + 'simplestates',
            headers: { Authorization: 'Bearer ' + BACKEND_TKN },
        };

        axios(config).then(function (response) {
            var promises = [];
            //collect data from api
            var states = response.data["states"];

            for(var i = 0; i < states.length; i++){
                var s_name = states[i]["name"];
                var s_id = states[i]["state_id"];
                id_to_name[s_id] = s_name;
                config = {
                    method: 'get',
                    url: BACKEND_BASE_URL + 'stateorgcount',
                    headers: { Authorization: 'Bearer ' + BACKEND_TKN },
                    params: {stateid: s_id}
                };

                promises.push(axios(config).then(function (response) {
                    var count = response.data["count"]["count"];
                    var id = response["config"]["params"]["stateid"];
                    data[id_to_name[id]] = count;
                }));
            }
            
            Promise.all(promises).then(function () {
                //form data for Recharts
                var output = [];
                var items = Object.keys(data).map(function(key) {
                    return [key, data[key]];
                  });
                  
                // Sort the array based on the second element
                items.sort(function(first, second) {
                return second[1] - first[1];
                });
                items = items.slice(0, 5)

                for(var i = 0; i < items.length; i++){
                    output.push({
                        state: items[i][0],
                        Organizations: items[i][1]
                    })
                }
                setData(output);
                setLoading(false);
            });
        });
    }, []);

    if (isLoading) {
        return (
            <div className="Model-Title">
                <Spinner animation="border" variant="light"></Spinner>
            </div>
        );
    }

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                color: 'white',
            }}
        >
            <BarChart
                width={900}
                height={400}
                data={data}
                margin={{
                top: 5, right: 30, left: 20, bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="state" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="Organizations" fill="#8884d8" />
            </BarChart>
        </div>
    );
};

export default OrganizationVisualization;