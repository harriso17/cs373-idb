import React from 'react';
import '../App.css';
import searchClient from '../constants';
import PagingTable from './PagingTable';
import Paginator from './Paginator';
import { Link } from 'react-router-dom';

import {
    InstantSearch,
    SearchBox,
    Highlight,
    Configure,
    connectHits,
    connectPagination,
    Index,
} from 'react-instantsearch-dom';

/**
 *  Component that displays pages of organizatons.
 */
export default function Search() {
    // creates a custom table, data is recieved in hits
    // uses paging table to format everything
    const PowerplantsHits = ({ hits }) => (
        <PagingTable
            header={['Name', 'Fuel', 'Owner', 'Latitude', 'Longitude']}
            body={hits.map((hit) => [
                <Link to={'Powerplants/' + hit.plant_id}>
                    {<Highlight attribute="plant_name" hit={hit} />}
                </Link>,
                <div>{<Highlight attribute="fuel" hit={hit} />}</div>,
                <div>{<Highlight attribute="owner" hit={hit} />}</div>,
                <div>
                    {<Highlight attribute="location.latitude" hit={hit} />}° W
                </div>,
                <div>
                    {<Highlight attribute="location.longitude" hit={hit} />}° N
                </div>,
            ])}
        />
    );

    const OrganizationsHits = ({ hits }) => (
        <PagingTable
            header={['Name', 'Description', 'Website', 'State', 'City']}
            body={hits.map((hit) => [
                <Link to={'Organizations/' + hit.organization_id}>
                    {<Highlight attribute="name" hit={hit} />}
                </Link>,
                <div className="hit-item" data-title={hit.description}>
                    {hit.description.substring(0, 16) + '...'}
                </div>,
                <a href={'https://' + hit.website}>
                    {<Highlight attribute="website" hit={hit} />}
                </a>,
                <div>{<Highlight attribute="state.name" hit={hit} />}</div>,
                <Link to={'Cities/' + hit.city_id}>
                    {<Highlight attribute="city.name" hit={hit} />}
                </Link>,
            ])}
        />
    );

    const CitiesHits = ({ hits }) => (
        <PagingTable
            header={['Name', 'State', 'Population', 'Latitude', 'Longitude']}
            body={hits.map((hit) => [
                <Link to={'Cities/' + hit.city_id}>
                    {<Highlight attribute="name" hit={hit} />}
                </Link>,
                <div>{<Highlight attribute="state.name" hit={hit} />}</div>,
                <div>{<Highlight attribute="population" hit={hit} />}</div>,
                <div>
                    {<Highlight attribute="location.latitude" hit={hit} />} ° W
                </div>,
                <div>
                    {<Highlight attribute="location.longitude" hit={hit} />} ° N
                </div>,
            ])}
        />
    );

    const PoliciesHits = ({ hits }) => (
        <PagingTable
            header={['Name', 'State', 'Description', 'Year Created', 'Type']}
            body={hits.map((hit) => [
                <Link to={'Policies/' + hit.policy_id}>
                    {<Highlight attribute="name" hit={hit} />}
                </Link>,
                <div>{<Highlight attribute="state.name" hit={hit} />}</div>,
                <div>{hit.description.substring(0, 16) + '...'}</div>,
                <div>{<Highlight attribute="created" hit={hit} />}</div>,
                <div>{<Highlight attribute="type_obj.name" hit={hit} />}</div>,
            ])}
        />
    );

    // connect custom components to instant search
    const PowerplantsCustomHits = connectHits(PowerplantsHits);
    const OrganizationsCustomHits = connectHits(OrganizationsHits);
    const CitiesCustomHits = connectHits(CitiesHits);
    const PoliciesCustomHits = connectHits(PoliciesHits);
    const CustomPaging = connectPagination(Paginator);

    // return the component to be rendered
    return (
        <div>
            <div className="Model-Title">
                <h1>Search</h1>
            </div>
            <div className="Model-Table">
                <InstantSearch
                    searchClient={searchClient}
                    indexName="powerplants"
                >
                    {/* only display 8 items per page */}
                    <Configure hitsPerPage={3} />

                    {/* search box component */}
                    <SearchBox translations={{ placeholder: 'Search...' }} />

                    {/* these are custom table and paging components */}
                    <h2>Organizations</h2>
                    <Index indexName="organizations">
                        <OrganizationsCustomHits />
                        <CustomPaging />
                    </Index>

                    <h2>Powerplants</h2>
                    <PowerplantsCustomHits />
                    <CustomPaging />

                    <h2>Cities</h2>
                    <Index indexName="cities">
                        <CitiesCustomHits />
                        <CustomPaging />
                    </Index>

                    <h2>Policies</h2>
                    <Index indexName="policies">
                        <PoliciesCustomHits />
                        <CustomPaging />
                    </Index>
                </InstantSearch>
            </div>
        </div>
    );
}
