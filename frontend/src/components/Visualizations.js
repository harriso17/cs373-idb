import React from 'react';
import EnergyVisualization from './visualizations/EnergyVisualization';
import PolicyVisualization from './visualizations/PolicyVisualization';
import EventVisualization from './visualizations/EventVisualization';
import OrganizationVisualization from './visualizations/OrganizationVisualization';
import NewsVisualization from './visualizations/NewsVisualizaation';
import VenueVisualization from './visualizations/VenueVisualization';

export default function Visualizations() {
    // return the component to be rendered
    return (
        <div>
            <div className="Model-Title">
                <h2>Number of Powerplants by Energy Type</h2>
            </div>
            <EnergyVisualization />
            <div className="Model-Title">
                <h2>Policies by Type</h2>
            </div>
            <PolicyVisualization />
            <div className="Model-Title">
                <h2>Top 5 States with Most Organizations</h2>
            </div>
            <OrganizationVisualization></OrganizationVisualization>
            <div className="Model-Title">
                <h2>[Provider] Events Count over Time</h2>
            </div>
            <EventVisualization />
            <div className="Model-Title">
                <h2>[Provider] News Events per City</h2>
            </div>
            <NewsVisualization></NewsVisualization>
            <div className="Model-Title">
                <h2>[Provider] Venues with Accessibility Options vs Venues Without</h2>
            </div>
            <VenueVisualization></VenueVisualization>
        </div>
    );
}
