import React from 'react';
import NavBar from './components/NavBar';
import Home from './components/Home';
import PowerPlants from './components/PowerPlants';
import Cities from './components/Cities';
import Organizations from './components/Organizations';
import Policies from './components/Policies';
import About from './components/About';
import Search from './components/Search';
import City from './components/instances/City';
import Organization from './components/instances/Organization';
import Policy from './components/instances/Policy';
import PowerPlant from './components/instances/PowerPlant';
import Visualizations from './components/Visualizations';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import ParticlesBg from 'particles-bg';

/**
 * Main application component.
 */
export default function App() {
    return (
        <div
            style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
            }}
        >
            <div>
                {/* Display NavBar */}
                <NavBar />

                <Switch>
                    <Route exact path="/About">
                        <About />
                    </Route>

                    <Route exact path="/Cities/:id">
                        <City />
                    </Route>
                    <Route exact path="/Cities">
                        <Cities />
                    </Route>

                    <Route exact path="/Organizations/:id">
                        <Organization />
                    </Route>
                    <Route exact path="/Organizations">
                        <Organizations />
                    </Route>

                    <Route exact path="/Policies/:id">
                        <Policy />
                    </Route>
                    <Route exact path="/Policies">
                        <Policies />
                    </Route>

                    <Route exact path="/PowerPlants/:id">
                        <PowerPlant />
                    </Route>
                    <Route exact path="/PowerPlants">
                        <PowerPlants />
                    </Route>
                    <Route exact path="/Search">
                        <Search />
                    </Route>
                    <Route exact path="/Visualizations">
                        <Visualizations />
                    </Route>
                    <Route path="/">
                        <Home />
                        <ParticlesBg color="#a0a0a0" type="cobweb" bg={true} />
                    </Route>
                </Switch>
            </div>
        </div>
    );
}
