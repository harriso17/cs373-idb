from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from unittest import main, TestCase
import requests



class UITests(TestCase):

    def setUp(self):
        self.PATH = "./chromedriver"
        self.driver = webdriver.Chrome(self.PATH)
        self.url = "https://dockerize.dflut22b6sl29.amplifyapp.com/"
        
    def test_home(self):

        self.driver.get(self.url)

        try:

            name = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'navbar-brand'))
            )
            assert 'PowerNet' == name.text
            time.sleep(0.5)
            navbar = self.driver.find_element_by_id("basic-navbar-nav")

            links = navbar.find_elements_by_class_name("nav-link")
            assert len(links) == 6

            api = self.driver.find_element_by_class_name('navbar-text')
            assert "API Documentation" == api.text

        finally:
            pass

    @staticmethod
    def model_page_test_helper(driver, url, pathname, tablename, nheaders, instancetextoverride=None):
        driver.get(url + pathname)

        try:

            title = WebDriverWait(driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Title'))
            )
            assert title.text == tablename

            table = WebDriverWait(driver, 5).until(
                EC.presence_of_element_located((By.TAG_NAME, 'table'))
            )
            time.sleep(0.5)
            table_head = table.find_element_by_tag_name('thead')
            headers = table_head.find_elements_by_tag_name('th')
            assert len(headers) == nheaders

            table_body = table.find_element_by_tag_name('tbody')

            instances = table_body.find_elements_by_tag_name('tr')
            assert len(instances) > 1

            pagination = driver.find_element_by_class_name('pagination')
            pages = pagination.find_elements_by_tag_name('li')
            assert len(pages) > 0

            link = instances[0].find_element_by_tag_name('a')
            linktext = link.text
            link.click()
            # in city instance

            instance = WebDriverWait(driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Title'))
            )
            if instancetextoverride is None:
                assert linktext == instance.text
            else:
                expected = instancetextoverride(linktext)
                assert expected == instance.text

        finally:
            pass

    def test_organizations(self):
        self.model_page_test_helper(self.driver, self.url, "Organizations", "Organizations", 5)

    def test_powerplants(self):
        self.model_page_test_helper(self.driver, self.url, "PowerPlants", "PowerPlants", 5)

    def test_cities(self):
        self.model_page_test_helper(self.driver, self.url, "Cities", "Cities", 5)

    def test_policies(self):
        self.model_page_test_helper(self.driver, self.url, "Policies", "Policies", 5, lambda x: x + "\nLearn more...")


    def test_city(self):
        self.driver.get(self.url + "Cities/28999")

        try:

            title = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Title'))
            )
            assert title.text == 'Mustang'

            table = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Table'))
            )
            rows = table.find_elements_by_tag_name('tr')
            assert len(rows) == 4

            expectedcontents = ['State Texas',
                                'Population 21',
                                'Longitude -96.4303° N',
                                'Latitude 32.0135° W']
            for i, r in enumerate(rows):
                assert r.text == expectedcontents[i]

        finally:
            pass


    def test_organization(self):
        self.driver.get(self.url + "Organizations/254")

        try:

            title = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Title'))
            )
            assert title.text == 'Pattern Energy'

            table = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Table'))
            )
            rows = table.find_elements_by_tag_name('tr')
            assert len(rows) == 2

            expectedcontents = ['City State',
                                'Albuquerque New Mexico']
            for i, r in enumerate(rows):
                assert r.text == expectedcontents[i]

        finally:
            pass

    def test_powerplant(self):
        self.driver.get(self.url + "PowerPlants/28847")

        try:

            title = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Title'))
            )
            assert title.text == 'Vaughn Creek PV1'

            table = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Table'))
            )
            rows = table.find_elements_by_tag_name('tr')
            assert len(rows) == 2

            expectedcontents = ['Fuel Capacity (MW) Commissioning Year Latitude Longitude',
                                'Solar 20 2018 36.451° W -77.1206° N']
            for i, r in enumerate(rows):
                assert r.text == expectedcontents[i]

        finally:
            pass

    def test_policy(self):
        self.driver.get(self.url + "Policies/4911")

        try:

            title = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Title'))
            )
            assert title.text == 'Harris County - Property Tax Abatement for Green ' \
                                 'Commercial Buildings\nLearn more...'

            table = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'Model-Table'))
            )
            rows = table.find_elements_by_tag_name('tr')
            assert len(rows) == 3

            expectedcontents = ['Year: 2011',
                                'Type: Property Tax Incentive',
                                'Affected State: Texas']
            for i, r in enumerate(rows):
                assert r.text == expectedcontents[i]

        finally:
            pass

    def test_about(self):

        self.driver.get(self.url + 'About')

        try:

            description = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.TAG_NAME, 'h1'))
            )
            assert description.text == 'Description'

            cards = self.driver.find_elements_by_class_name('card')
            assert len(cards) == 5

            h2s = self.driver.find_elements_by_tag_name('h2')
            assert len(h2s) == 3

            tables = self.driver.find_elements_by_tag_name('table')
            for t in tables:
                tbody = t.find_element_by_tag_name('tbody')
                trows = tbody.find_elements_by_tag_name('tr')
                assert len(trows) > 0

        finally:
            pass

    def test_about(self):

        self.driver.get(self.url + 'About')

        try:

            description = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.TAG_NAME, 'h1'))
            )
            assert description.text == 'Description'

            cards = self.driver.find_elements_by_class_name('card')
            assert len(cards) == 5

            h2s = self.driver.find_elements_by_tag_name('h2')
            assert len(h2s) == 3

            tables = self.driver.find_elements_by_tag_name('table')
            for t in tables:
                tbody = t.find_element_by_tag_name('tbody')
                trows = tbody.find_elements_by_tag_name('tr')
                assert len(trows) > 0

        finally:
            pass

    def test_organizations(self):

        self.driver.get(self.url + 'Organizations')

        try:

            header = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.TAG_NAME, 'h1'))
            )
            assert header.text == 'Organizations'

            table = WebDriverWait(self.driver, 5).until(
                EC.presence_of_element_located((By.TAG_NAME, 'table'))
            )
            time.sleep(0.5)
            table_head = table.find_element_by_tag_name('thead')
            headers = table_head.find_elements_by_tag_name('th')
            assert len(headers) == 5

            table_body = table.find_element_by_tag_name('tbody')
            orgs = table_body.find_elements_by_tag_name('tr')
            assert len(orgs) > 1

            pagination = self.driver.find_element_by_class_name('pagination')
            pages = pagination.find_elements_by_tag_name('li')
            assert len(pages) > 0


        finally:
            pass


if __name__ == "__main__":
    
    main()
        
           
