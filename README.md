## Members
Travis Eakin (tke246 | @eakintk)
Carlo Desantis (ccd2238 | @cdesan)
Harrison Gross (rhg555 | @harriso17)
Sean Chen (sc53864 | @sean.chen)
Alexander Greason (abg2352 | @alexgreason1)

## Git SHA
968ab99d7ddc52ddef41d615051b421989d3804a

## Project Leader
Collective

## GitLab Pipelines
[https://gitlab.com/harriso17/cs373-idb/-/pipelines](https://gitlab.com/harriso17/cs373-idb/-/pipelines)

## Website
[powernet.energy](https://www.powernet.energy/)

## Estimated Time and Actual Time (in hours)
Travis Eakin - estimated: 10 actual: 3
Carlo Desantis - estimated: 5 actual: 5
Harrison Gross - estimated: 15 actual: 18
Sean Chen - estimated: 20 actual: 4
Alexander Greason - estimated: 7 actual: 5

## Comments

# Run at project root
docker-compose up -d --build
