import datetime

from extensions import db
from passlib.apps import custom_app_context as pwd_context
from flask import Blueprint,request,g,jsonify
from itsdangerous import (BadSignature,
SignatureExpired, JSONWebSignatureSerializer as Serializer)

from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth

import os

auth = HTTPBasicAuth()
auth_token = HTTPTokenAuth()
s = Serializer(secret_key=os.environ.get('SECRET_KEY'))

class User(db.Model):
    """ User Model for storing user related details """
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.Unicode, nullable=False)
    password_hash = db.Column(db.Unicode, nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, username, password):
        self.username = username
        self.password_hash = pwd_context.encrypt(password)
        self.registered_on = datetime.datetime.now()

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self):
        return s.dumps({ 'id': self.id })

    # Method that verifies the validity of a token per user
    @staticmethod
    def verify_auth_token(token):
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = User.query.get(data['id'])
        return user

    

# Blueprints for flask. Breaks the entire app into smaller portions. 
# Only one portion right now, 'api' that will do all of the work
auth_bp = Blueprint("api/auth", __name__)

#Route implementations
# This route uses the username/password credentials to obtain a token.
@auth_bp.route('/api/auth/users', methods=['POST', 'GET', 'DELETE'])
@auth_token.login_required
def handle_user():
    if request.method == 'POST':
        if request.is_json:
            data = request.get_json()
            user = User(username=data['username'], password=data['password'])
            db.session.add(user)
            db.session.commit()
            return {"message": f"User {user.username} has been created."}
        else:
            return {"error": "The request payload is not in JSON format"}

    elif request.method == 'GET':
        users = User.query.all()
        results = [
            {
                "username": user.username
            } for user in users]

        return {"count": len(results), "users": results}

# Password verification
@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True

# Token verification
@auth_token.verify_token
def verify_token(token):
    user = User.verify_auth_token(token)
    if not user:
        return False
    g.user = user
    return True

# Route for grabbing a token
@auth_bp.route('/api/auth/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token()
    return jsonify({ 'token': token.decode('ascii') })