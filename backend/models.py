# File to contain all of the defined db models

from extensions import db

class State(db.Model):

    state_id = db.Column(db.Integer, primary_key=True)
    short_form = db.Column(db.Unicode)
    name = db.Column(db.Unicode)

class Location(db.Model):
    location_id = db.Column(db.Integer, primary_key=True)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    solar_illuminance = db.Column(db.Float)
    image_url = db.Column(db.Unicode)


class City(db.Model):
    city_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)
    state_id = db.Column(db.Integer, db.ForeignKey('state.state_id'))
    state = db.relationship(State, backref=db.backref('cities'))
    population = db.Column(db.Integer)
    location_id = db.Column(db.Integer, db.ForeignKey('location.location_id'))
    location = db.relationship(Location, backref=db.backref('cities'))

class Organization(db.Model):
    organization_id = db.Column(db.Integer, primary_key=True)
    logo_url = db.Column(db.Unicode)
    description = db.Column(db.Unicode)
    website = db.Column(db.Unicode)
    name = db.Column(db.Unicode)
    headquarters_location_id = db.Column(db.Integer,
    db.ForeignKey('location.location_id'))
    headquarters_location = db.relationship(Location,
    backref=db.backref('organizations'))

    city_id = db.Column(db.Integer, db.ForeignKey('city.city_id'))
    city = db.relationship(City, backref=db.backref('organizations'))

    state_id = db.Column(db.Integer, db.ForeignKey('state.state_id'))
    state = db.relationship(State, backref=db.backref('organizations'))

class PowerPlant(db.Model):
    __tablename__ = 'powerplant'
    plant_id = db.Column(db.Integer, primary_key=True)
    org_id = db.Column(db.Integer,
    db.ForeignKey('organization.organization_id'))
    org = db.relationship(Organization, backref=db.backref('powerplants'))
    plant_name = db.Column(db.Unicode)
    capacity = db.Column(db.Float)
    fuel = db.Column(db.Unicode)
    commission_year = db.Column(db.Integer)
    image_url = db.Column(db.Unicode)
    location_id = db.Column(db.Integer, db.ForeignKey('location.location_id'))
    location = db.relationship(Location, backref=db.backref('powerplants'))
    owner = db.Column(db.Unicode)

class EnergySource(db.Model):
    __tablename__ = 'energysource'
    source_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)

class Entity(db.Model):
    prod_type_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)

class Emission(db.Model):
    emission_id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.Integer, db.ForeignKey('state.state_id'))
    state_obj = db.relationship(State, backref=db.backref('emissions'))
    year = db.Column(db.Integer)
    co2 = db.Column(db.Integer)
    so2 = db.Column(db.Integer)
    nox = db.Column(db.Integer)
    producer_type_id = db.Column(db.Integer,
    db.ForeignKey('entity.prod_type_id'))
    producer = db.relationship(Entity, backref=db.backref('emissions'))
    source_id = db.Column(db.Integer, db.ForeignKey('energysource.source_id'))
    source = db.relationship(EnergySource, backref=db.backref('emissions'))
    hydroelectric = db.Column(db.Integer)
    other_gases = db.Column(db.Integer)
    natural_gas = db.Column(db.Integer)
    nuclear = db.Column(db.Integer)
    geothermal = db.Column(db.Integer)
    solar = db.Column(db.Integer)
    wind = db.Column(db.Integer)
    wood = db.Column(db.Integer)
    petroleum = db.Column(db.Integer)
    other_biomass = db.Column(db.Integer)
    other = db.Column(db.Integer)
    pumped_storage = db.Column(db.Integer)
    coal = db.Column(db.Integer)

class PolicyProgramType(db.Model):
    __tablename__ = 'policyprogramtype'
    policy_type_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)

class Policy(db.Model):
    policy_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode)
    description = db.Column(db.Unicode)
    website_url = db.Column(db.Unicode)
    created = db.Column(db.Integer) # should this be a Year?
    type_id = db.Column(db.Integer,
    db.ForeignKey('policyprogramtype.policy_type_id'))
    type_obj = db.relationship(PolicyProgramType,
    backref=db.backref('policies'))
    state_id = db.Column(db.Integer, db.ForeignKey('state.state_id'))
    state = db.relationship(State, backref=db.backref('policies'))

class CityPolicy(db.Model):
    __tablename__ = "citypolicy"
    id = db.Column(db.Integer, primary_key=True)
    city_id = db.Column(db.Integer, db.ForeignKey('city.city_id'))
    policy_id = db.Column(db.Integer, db.ForeignKey('policy.policy_id'))