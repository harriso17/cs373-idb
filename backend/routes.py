# Additional routes that require custom SQL queries

from flask import Blueprint,request,jsonify
from models import *
from extensions import db
from auth import auth_token
from flask_cors import cross_origin

# Blueprints for flask. Breaks the entire app into smaller portions.
# Only one portion right now, 'api' that will do all of the work
api = Blueprint("api", __name__)

#Route implementations

@api.route('/')
def hello():
    return {"hello": "world"}

@api.route('/api/powerplantlocation', methods=['GET'])
@auth_token.login_required
def handle_plantlocation():
    if request.method == 'GET':
        resultproxy = db.engine.execute("select * from powerplantlocation")
        d, a = {}, []
        for rowproxy in resultproxy:
            for column, value in rowproxy.items():
                # build up the dictionary
                d = {**d, **{column: value}}
            a.append(d)
        return {"powerplants": a}

@api.route('/api/citylocation', methods=['GET'])
@auth_token.login_required
def handle_citylocation():
    if request.method == 'GET':
        query = """
                select c.name as city_name, s.name as state_name,
                c.population, l.latitude, l.longitude 
                from city as c 
                join location as l 
                on c.location_id = l.location_id 
                join state as s 
                on c.state_id = s.state_id
                """
        resultproxy = db.engine.execute(query)
        d, a = {}, []
        for rowproxy in resultproxy:
            for column, value in rowproxy.items():
                # build up the dictionary
                d = {**d, **{column: value}}
            a.append(d)
        return {"cities": a}

@api.route('/api/statename', methods=['GET'])
@auth_token.login_required
def handle_statename():
    if request.method == 'GET':
        query = "select name from state where state_id = " + request.args.get("stateid")
        resultproxy = db.engine.execute(query)
        d, a = {}, []
        for rowproxy in resultproxy:
            for column, value in rowproxy.items():
                # build up the dictionary
                d = {**d, **{column: value}}
            a.append(d)
        return {"name": a}

@api.route('/api/stateorgcount', methods=['GET'])
@auth_token.login_required
def handle_statecount():
    if request.method == 'GET':
        query = "select count(o.name) from organization as o where o.state_id = " + request.args.get("stateid")
        resultproxy = db.engine.execute(query)
        d, a = {}, []
        for rowproxy in resultproxy:
            for column, value in rowproxy.items():
                # build up the dictionary
                d = {**d, **{column: value}}
            a.append(d)
        a = a[0]
        return {"count": a}

@api.route('/api/simplestates', methods=['GET'])
@auth_token.login_required
@cross_origin()
def handle_simplestates():
    if request.method == 'GET':
        query = "select s.state_id, s.name from state as s"
        resultproxy = db.engine.execute(query)
        d, a = {}, []
        for rowproxy in resultproxy:
            for column, value in rowproxy.items():
                # build up the dictionary
                d = {**d, **{column: value}}
            a.append(d)
        return {"states": a}