import unittest
from application import create_app
from config import testConfig

class AuthTests(unittest.TestCase):

    def setUp(self):
        app = create_app(testConfig)
        self.app = app.test_client()


    def test_home_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.app.get('/') 

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)


    def test_unauthorized_status_code(self):

        result = self.app.get('/api/state')

        self.assertEqual(result.status_code, 401)

class AppTests(unittest.TestCase):

    def setUp(self):
        app = create_app(testConfig)
        self.app = app.test_client()
        self.headers = {"Authorization": "Bearer " + 
        "eyJhbGciOiJIUzUxMiJ9.eyJpZCI6M30.ZEJnN_EHhd0buYwWS9eQ4pZwcYIl-VycT_JfMysF7AJvwfrZfTgw5G_eIT69MQy-7jp-5b23rQtOnJXPwpUniw"}


    def test_home_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.app.get('/') 

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_state_end(self):
        result = self.app.get('/api/state', headers=self.headers)
        self.assertEqual(result.status_code, 200)

    def test_location_end(self):
        result = self.app.get('/api/location', headers=self.headers)
        self.assertEqual(result.status_code, 200)
    
    def test_powerplant_end(self):
        result = self.app.get('/api/powerplant', headers=self.headers)
        self.assertEqual(result.status_code, 200)
    
    def test_policytype_end(self):
        result = self.app.get('/api/policyprogramtype', headers=self.headers)
        self.assertEqual(result.status_code, 200)

    def test_org_end(self):
        result = self.app.get('/api/organization', headers=self.headers)
        self.assertEqual(result.status_code, 200)

    def test_city_end(self):
        result = self.app.get('/api/city', headers=self.headers)
        self.assertEqual(result.status_code, 200)

    def test_emission_end(self):
        result = self.app.get('/api/emission', headers=self.headers)
        self.assertEqual(result.status_code, 200)



def create_suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(AuthTests())
    test_suite.addTest(AppTests())
    return test_suite

if __name__ == '__main__':
   suite = unittest.defaultTestLoader.loadTestsFromTestCase(AuthTests)
   unittest.TextTestRunner().run(suite)

   suite = unittest.defaultTestLoader.loadTestsFromTestCase(AppTests)
   unittest.TextTestRunner().run(suite)
