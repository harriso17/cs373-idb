# Consolidates everything here and launches the applicaton

from flask import Flask,request,g,after_this_request
from flask_cors import CORS
from config import config
from models import *

# Wrapper method for the authentication system.
# Provides Flask-Restless a method of using authentication
def auth_func(*args, **kw):
    from flask_restless import ProcessingException
    from auth import verify_token

    auth_val = request.headers.get('Authorization')
    if(auth_val is not None):
        token = auth_val[7:]

        if(verify_token(token)):
            return True
    desc = f'Not authorized. Header: {auth_val}. Method: {request.method}'
    raise ProcessingException(description=desc, code=401)

# Creates the flask application and registers the externsions
# and additional blueprints for api.
def create_app(configuration):
    app = Flask(__name__)
    CORS(app, support_credentials=True)
    app.config.from_object(configuration)
    register_extensions(app)
    register_blueprints(app)
    return app

# Registers the blueprints from other files
def register_blueprints(app):
    from auth import auth_bp
    from routes import api
    app.register_blueprint(auth_bp)
    app.register_blueprint(api)
    return

# Setup for flask extensions
def register_extensions(app):
    from extensions import db
    from extensions import migrate
    from extensions import apimanager
    #from auth import verify_token

    db.init_app(app)

    migrate.init_app(app, db)

    preprocessors=dict(
        GET_MANY=[auth_func,],
        GET_SINGLE=[auth_func,],
        POST=[auth_func,],
        PUT_SINGLE=[auth_func,],
        PUT_MANY=[auth_func,],
        DELETE_SINGLE=[auth_func,],
        DELETE_MANY=[auth_func,])

    with app.app_context():
        db.create_all()
        create_apis(apimanager)
        apimanager.init_app(
        app, flask_sqlalchemy_db=db,
        preprocessors=preprocessors)

# Method for storing all of the calls to create_api
def create_apis(api):
    api.create_api(State, methods=['GET'])
    api.create_api(Location, methods=['GET'])
    api.create_api(PowerPlant, methods=['GET'])
    api.create_api(PolicyProgramType, methods=['GET'])
    api.create_api(Organization, methods=['GET'])
    api.create_api(City, methods=['GET'])
    api.create_api(Emission, methods=['GET'])
    api.create_api(Policy, methods=['GET'])
    api.create_api(CityPolicy, methods=['GET'])




application = create_app(config)

if __name__ == '__main__':
    application.run(host='0.0.0.0')
