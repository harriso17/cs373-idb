# Purpose of file is to prevent circular imports with db and other flask extensions

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import Flask
from flask_restless import APIManager

db = SQLAlchemy()
migrate = Migrate()
apimanager = APIManager()

