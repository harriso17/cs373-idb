#Configurations for different versions of app
import os
class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = "postgres+psycopg2://postgres:idbdevelopment@idb-dev.cx6mrq4auhyg.us-east-2.rds.amazonaws.com:5432/postgres"
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    CORS_HEADERS = 'Content-Type'

config = Config

class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "postgres+psycopg2://postgres:idbdevelopment@idb-dev.cx6mrq4auhyg.us-east-2.rds.amazonaws.com:5432/postgres"


testConfig = TestingConfig
